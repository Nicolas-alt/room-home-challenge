const hamburger = document.getElementById('hamburger')
const menuUl = document.getElementById('ulContainer')
const closeHbger = document.getElementById('hamburgerClose')
const divLayout = document.getElementById('divLayout')

hamburger.addEventListener('click', () => {
    menuUl.classList.toggle('ul--dBlock')
    divLayout.classList.toggle('div--screenBlock')
    closeHbger.classList.toggle('img--iconOpen')
})

closeHbger.addEventListener('click', () => { 
    menuUl.classList.toggle('ul--dBlock') 
    divLayout.classList.toggle('div--screenBlock')
    closeHbger.classList.toggle('img--iconOpen')
})